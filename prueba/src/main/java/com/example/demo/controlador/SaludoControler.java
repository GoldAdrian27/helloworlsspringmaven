package com.example.demo.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SaludoControler {

	@RequestMapping("/hola")
	public String hola() {
		return "hola";
	}
}
